import irc.bot
import datetime
import random
import threading
import time
import os

mots = []

def load_words():
    global mots
    mots = []
    with open('words.txt') as words:
        for line in words.readlines():
            mots.append(line.strip())
    return len(mots)

def cranscrans(conn, channel):
    while True:
        hour = datetime.datetime.now().hour
        if hour < 1 or hour > 11:
            try:
                say_crans(conn, channel)
            except:
                pass
        t = time.time() + 5400 + random.randint(0, 4*3600)
        while t > time.time():
            time.sleep(60)

def say_crans(conn, channel):
    global mots
    say_crans.last_words = []
    i = random.randint(0, len(mots)-1)
    say_crans.last_words.append(i)
    if len(say_crans.last_words) > 10:
        del say_crans.last_words[0]
    conn.privmsg(channel, "Crans Crans " + mots[i] + " !")


class Bot(irc.bot.SingleServerIRCBot):

    def __init__(self, nickname="newcrans", channel="#newcrans", server="irc.crans.org", port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.nick = nickname
        self.t = None

    def on_nicknameinuse(self, conn, e):
        conn.nick(conn.get_nickname() + "_")
        self.nick += "_"

    def on_pubmsg(self, conn, e):
        m = e.arguments[0]
        if m.strip() == self.nick + ': encore':
            say_crans(conn, self.channel)
        if m.strip() == self.nick + ': update':
            os.system('git pull')
            conn.privmsg(self.channel, '{0} rimes au total.'.format(load_words()))

    def on_welcome(self, conn, e):
        conn.join(self.channel)
        if not self.t:
            self.t = threading.Thread(target=cranscrans, args=(conn, self.channel))
            self.t.start()


if __name__ == '__main__':
    load_words()
    encore = Bot()
    encore.start()
